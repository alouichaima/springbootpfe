package com.pfe.fitness.services;

import java.util.List;

import com.pfe.fitness.entities.Activite;


public interface ActiviteService {
	
	public List<Activite> getAllActivite();
	public Activite findActiviteById (Long id);
	public Activite createActivite( Activite activite);
	public Activite updateActivite( Activite activite);
	public void deleteActivite( long id);
	
	
	public long save(Activite act);
	public String deleteActivite(String nomA);

}
