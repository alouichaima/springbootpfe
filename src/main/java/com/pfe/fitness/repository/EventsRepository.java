package com.pfe.fitness.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pfe.fitness.entities.Events;

@Repository
public interface EventsRepository extends JpaRepository <Events, Long> {

}
