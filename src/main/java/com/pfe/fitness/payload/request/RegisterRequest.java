package com.pfe.fitness.payload.request;

import java.sql.Date;
import java.util.Set;

import javax.validation.constraints.*;
 
public class RegisterRequest {
	
	@Size(max = 20)
	private String nom;
	
	@Size(max = 20)
	private String prenom;
	
   
    private Date datenaiss;
 
    @NotBlank
    @Size(max = 50)
    @Email
    private String email;
    
    private Set<String> role;
    
    @NotBlank
    @Size(min = 6, max = 40)
    private String password;
    
   
	private int telephone;
	
	
	@Size(max = 20)
	private String typesport;
	
	
	private double poids;
	
	
	
	


	
	public RegisterRequest(@Size(max = 20) String nom, @Size(max = 20) String prenom,
			 Date datenaiss, @NotBlank @Size(max = 50) @Email String email,
			Set<String> role, @NotBlank @Size(min = 6, max = 40) String password,
			 int telephone,  @Size(max = 20) String typesport,
			 double poids) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.datenaiss = datenaiss;
		this.email = email;
		this.role = role;
		this.password = password;
		this.telephone = telephone;
		this.typesport = typesport;
		this.poids = poids;
	}







	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}


	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public Date getDatenaiss() {
		return datenaiss;
	}

	public void setDatenaiss(Date datenaiss) {
		this.datenaiss = datenaiss;
	}

	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public Set<String> getRole() {
		return role;
	}

	public void setRole(Set<String> role) {
		this.role = role;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getTelephone() {
		return telephone;
	}

	public void setTelephone(int telephone) {
		this.telephone = telephone;
	}

	public String getTypesport() {
		return typesport;
	}

	public void setTypesport(String typesport) {
		this.typesport = typesport;
	}

	public double getPoids() {
		return poids;
	}

	public void setPoids(double poids) {
		this.poids = poids;
	}

	
  
	
    
}
